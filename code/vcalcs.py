from math import sqrt, acos, atan2, cos, sin, pi


class Vector:
    def __init__(self, a, b, c):
        self.x = a
        self.y = b
        self.z = c

    def cross(self, other):
        nx = self.y*other.z - self.z*other.y
        ny = self.z*other.x - self.x*other.z
        nz = self.x*other.y - self.y*other.x
        return Vector(nx, ny, nz)

    def dot(self, other):
        return self.x*other.x + self.y*other.y + self.z*other.z

    def r(self):
        return sqrt(self.dot(self))

    def coords(self):
        return [self.x, self.y, self.z]

    def x(self):
        return self.x

    def y(self):
        return self.y

    def z(self):
        return self.z

    def __str__(self):
        return '('+str(self.x)+','+str(self.y)+','+str(self.z)+')'

    def subtract(self, other):
        return Vector(self.x-other.x, self.y-other.y, self.z-other.z)

    def scale(self, a):
        return Vector(self.x*a, self.y*a, self.z*a)


class Matrix:
    def __init__(self, aa, ab, ac, ba, bb, bc, ca, cb, cc):
        self._grid = [[aa, ab, ac], [ba, bb, bc], [ca, cb, cc]]

    def affect(self, vec):
        v = vec.coords()
        newv = [0, 0, 0]
        for i in range(3):
            newv[i] = 0
            for j in range(3):
                newv[i] += v[j]*self._grid[i][j]
        return Vector(newv[0], newv[1], newv[2])

    def T(self):
        return Matrix(
            self._grid[0][0], self._grid[1][0], self._grid[2][0],
            self._grid[0][1], self._grid[1][1], self._grid[2][1],
            self._grid[0][2], self._grid[1][2], self._grid[2][2])


def tau_sign(ra, rm):
    # For a given two points and an eccentricity,
    # two ellipses are actually possible.
    # These only differ by the sign used whenever tau is added,
    # which is can be determined on a case-by-case basis.
    # This sign is computed by this function.
    thetara = atan2(ra.y(), ra.x())
    thetarm = atan2(rm.y(), rm.x())
    delta = thetarm-thetara
    if delta > pi:
        return -1
    if delta < 0 or delta > pi:
        return -1
    return 1


def costau_compute(ra, rm, e, ts):
    # Handy short-hand for commonly use variables.
    p = ra.r()
    q = e*p
    m = rm.r()
    n = e*m
    # Cosine and Sine of the angles between the vectors.
    costheta = ra.dot(rm)/(p*m)
    sintheta = sqrt(1-costheta*costheta)
    # Compute cos(tau),
    # where tau is the angle between b and periapsis.
    # This is a large computation, and is done in parts.
    costaunumsqrt = sqrt(n*n - m*m + 2*m*p - p*p + q*q - 2*n*q*costheta)
    costaunum = m*q - p*q + n*(p-m)*costheta + ts*n*sintheta*costaunumsqrt
    costauden = n*n + q*q - 2*n*q*costheta
    costau = costaunum / costauden
    # This is a cosine value, and should not be outside of [-1,1].
    # Thus, we bound it.
    return max(-1, min(1, costau))


def time(ra, rm, e, ts):
    # Compute the cosine of the angle between the burn point and the periapsis.
    costau = costau_compute(ra, rm, e, ts)
    # Ratio between the semi-minor and semi-major axes.
    # Used a lot, and thus memoized.
    axisratio = abs(1-e*e)
    axissqrt = sqrt(axisratio)
    # Semi-major axis
    a = ra.r()*(1+e*costau)/axisratio
    # Rotate the points so that the periapsis lies on the x-axis.
    # This is done by first moving the burn point there,
    # and then rotating by tau.
    rotfac = -atan2(ra.y(), ra.x()) - ts*tau_sign(ra, rm)*acos(costau)
    rotc = cos(rotfac)
    rots = sin(rotfac)
    Rot = Matrix(rotc, -rots, 0,
                 rots, rotc,  0,
                 0,    0,     1)
    rarot = Rot.affect(ra)
    rmrot = Rot.affect(rm)
    # Scale the ellipse into a circle.
    # This is so that area swept out can be found.
    # This allows the amount of time travelled to be computed.
    rascale = Vector((rarot.x()+e*a)*axissqrt, rarot.y(), 0)
    rmscale = Vector((rmrot.x()+e*a)*axissqrt, rmrot.y(), 0)
    # Use Kepler's Third Law to compute the time it takes to sweep 1 radian.
    GM = 0.000296005155
    areaperrad = sqrt(a*a*a/GM)
    # Ideally, rascale and rmscale have the same magnitude, being on a circle.
    # This is merely a way of minimizing issues if they somehow aren't.
    r2 = rascale.r()*rascale.r()
    # The area swept out around the shifted focus,
    # can be computed by adding the area swept out around the centre,
    # and a quadrilateral, (rascale)O(rmscale)(shifted focus).
    # This latter quadrilateral can have its area
    # easily computed as two triangles.
    sector = acos(rascale.dot(rmscale)/r2)*r2
    quad = e*a*axissqrt*(rascale.y()-rmscale.y())
    return areaperrad*(sector+quad)/r2


def emin(b, c):
    # Return a minimum for what eccentricity could be.
    # This is definitely a possible value for it,
    # and eccentricity cannot fall below it.
    return abs((b.r()-c.r())/(b.subtract(c).r()))


def newton(ra, rm, t, ts):
    # Establish a minimum value for eccentricity,
    # which we use to establish bounds.
    bote = emin(ra, rm)
    # time(eccentricity), in some cases,
    # is not strictly increasing.
    # It is often first decreasing, then increasing,
    # and in some cases is decreasing near the max of 1.
    # Thus, we determine in what interval it is increasing,
    # and do Newton's method only there.
    # These bounds are stored in bot and top.
    res = 10000
    prev = time(ra, rm, (bote*(res-1)+1)/res, ts)
    bot = bote
    for i in range(2, res-1):
        new = time(ra, rm, (bote*(res-i)+i)/res, ts)
        if new > prev:
            bot = (bote*(res-i)+i)/res
            break
        prev = new
    top = 1
    prev = time(ra, rm, (bote+res-1)/res, ts)
    for i in range(res-2, 1, -1):
        new = time(ra, rm, (bote*(res-i)+i)/res, ts)
        if new < prev:
            top = (bote*(res-i)+i)/res
            break
    # Establish step length.
    # This is used for local linear approximation of derivatives.
    h = (1-bote)/10000000
    # Select an initial point,
    # using a weighted average.
    weight = 0.5
    # Perform Newton's method,
    # using local linear approximation to find derivatives.
    # This determines the eccentricity needed for a certain time interval t.
    cur = (1-weight)*top + weight*bot
    curt = time(ra, rm, cur, ts)
    curth = time(ra, rm, cur+h, ts)
    for i in range(10):
        cur = max(bot + 10**(-i-6),
                  min(cur - (curt-t)*h/(curth-curt), top - 10**(-i-6)))
        curt = time(ra, rm, cur, ts)
        curth = time(ra, rm, cur+h, ts)
    return cur


def vcalc(ra, mr, t, ts):
    # Construct the normal vector to the plane.
    n = mr.cross(ra)
    # nq is the xy-projection of n.
    nq = sqrt(n.x()*n.x() + n.y()*n.y())
    nr = n.r()
    # Rotate the vectors into the xy plane.
    R1 = Matrix(n.x()/nq,  n.y()/nq, 0,
                -n.y()/nq, n.x()/nq, 0,
                0,        0,       1)
    R2 = Matrix(n.z()/nr, 0, -nq/nr,
                0,       1, 0,
                nq/nr,     0, n.z()/nr)
    rmrot = R2.affect(R1.affect(mr))
    rarot = R2.affect(R1.affect(ra))
    # Obtain the eccentricity for the desired travel time,
    # and construct the rotated orbit of the vector.
    e = newton(rarot, rmrot, t, ts)
    tau = costau_compute(rarot, rmrot, e, ts)
    a = rarot.r()*(1+e*tau)/(1-e*e)
    # angle between the two vectors.
    theta = atan2(rmrot.y(), rmrot.x()) - atan2(rarot.y(), rarot.x())
    # Calculate velocity unit vector in the rotated orbit.
    # v indicates velocity at burn point,
    # w indicates velocity at collision point.
    vroty = -tau-e
    vrotx = ts*tau_sign(rmrot, rarot)*sqrt(1-tau*tau)
    vrotmag = sqrt(vroty*vroty + vrotx*vrotx)
    vrot = Vector(vrotx/vrotmag, vroty/vrotmag, 0)
    wroty = -cos(theta+ts*tau_sign(rmrot, rarot)*acos(tau))-e
    wrotx = sin(theta+ts*tau_sign(rmrot, rarot)*acos(tau))
    wrotmag = sqrt(wroty*wroty + wrotx*wrotx)
    wrot = Vector(wrotx/wrotmag, wroty/wrotmag, 0)
    # Construct the inverse rotational matrices,
    # from the periapsis at x-axis to initial position.
    rotfactor = (-atan2(rarot.y(), rarot.x()) -
                 ts*tau_sign(rarot, rmrot)*acos(tau))
    rotcos = cos(rotfactor)
    rotsin = sin(rotfactor)
    Rot = Matrix(rotcos, -rotsin, 0,
                 rotsin, rotcos,  0,
                 0,    0,     1).T()
    R2T = R2.T()
    R1T = R1.T()
    # Construct velocity unit vectors.
    vhat = R1T.affect(R2T.affect(Rot.affect(vrot)))
    what = R1T.affect(R2T.affect(Rot.affect(wrot)))
    # Scale velocity vectors.
    GM = 0.000296005155
    vmag = sqrt(GM*(2/ra.r()-1/a))
    wmag = sqrt(GM*(2/mr.r()-1/a))
    v = vhat.scale(vmag)
    w = what.scale(wmag)
    return v, w


mr = {}
mv = {}
maxt = 0
with open('../data/csv/mars.csv', 'r') as mars_file:
    mars_file.readline()
    for end in mars_file:
        mdat = end.split(',')
        mt = int(float(mdat[0]))
        mr[mt] = Vector(float(mdat[1]), float(mdat[2]), float(mdat[3]))
        mv[mt] = Vector(float(mdat[4]), float(mdat[5]), float(mdat[6]))
        if mt > maxt:
            maxt = mt
for ts in [1, -1]:
    for x in range(501, 4000):
        print(x)
        try:
            with open('../data/csv/'+str(x)+'.csv', 'r') as eph_file:
                with open('../data/out/'+str(x)+'.csv', 'a') as out_file:
                    eph_file.readline()
                    counter = 0
                    for start in eph_file:
                        counter += 1
                        print('\t'+str(counter))
                        dat = start.split(',')
                        t = int(float(dat[0]))
                        # mt, E, I
                        bestE = [-1, -1, -1]
                        bestI = [-1, -1, -1]
                        bestD = [-1, -1, -1]
                        benchmark2 = 1
                        counter2 = 0
                        for k in range(1, (maxt-t)//5, 30):
                            mt = t + k
                            m = mr[mt]
                            er = Vector(float(dat[1]),
                                        float(dat[2]),
                                        float(dat[3]))
                            ev = Vector(float(dat[4]),
                                        float(dat[5]),
                                        float(dat[6]))
                            try:
                                v, v2 = vcalc(m, er, mt-t, ts)
                                dv = v.subtract(ev)
                                dv2 = v2.subtract(mv[mt])
                                E = dv2.dot(dv2)
                                I = dv.dot(dv)
                                if E > bestE[1]:
                                    bestE[0] = mt
                                    bestE[1] = E
                                    bestE[2] = I
                                if I < bestI[2] or bestI[2] == -1:
                                    bestI[0] = mt
                                    bestI[1] = E
                                    bestI[2] = I
                                if E - I > bestD[1] - bestD[2]:
                                    bestD[0] = mt
                                    bestD[1] = E
                                    bestD[2] = I
                            except(ValueError):
                                pass
                        # format: start, target, E, I, ts
                        out_file.write(str(start) + ',' +
                                       str(bestE[0]) + ',' +
                                       str(bestE[1]) + ',' +
                                       str(bestE[2]) + ',' +
                                       str(ts)+'\n')
                        out_file.write(str(start) + ',' +
                                       str(bestI[0]) + ',' +
                                       str(bestI[1]) + ',' +
                                       str(bestI[2]) + ',' +
                                       str(ts)+'\n')
                        out_file.write(str(start) + ',' +
                                       str(bestD[0]) + ',' +
                                       str(bestD[1]) + ',' +
                                       str(bestD[2]) + ',' +
                                       str(ts)+'\n')
        except:
            print('Failed to load.')
