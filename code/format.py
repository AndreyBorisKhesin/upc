fails = 0
for i in range(1, 4000):
    try:
        with open('../data/'+str(i)+'.eph', 'r') as eph_file:
            with open('../data/csv/'+str(i)+'.csv', 'w') as out_file:
                out_file.write('t,x,y,z,vx,vy,vz,lt,rg,rr\n')
                eph_lines = []
                start = False
                for line in eph_file:
                    if line.startswith("$$EOE"):
                        start = False
                    if start:
                        eph_lines.append(line.strip().replace('  ', ' '))
                    if line.startswith("$$SOE"):
                        start = True
                for j in range(0, len(eph_lines)//20, 30):
                    num = int(float(eph_lines[4*j][:eph_lines[4*j].find("=")]))
                    xyz = eph_lines[4*j+1].split(' ')
                    v = eph_lines[4*j+2].split(' ')
                    rest = eph_lines[4*j+3].split(' ')
                    out_file.write(str(num)+',')
                    out_file.write(str(xyz[0])+',')
                    out_file.write(str(xyz[1])+',')
                    out_file.write(str(xyz[2])+',')
                    out_file.write(str(v[0])+',')
                    out_file.write(str(v[1])+',')
                    out_file.write(str(v[2])+',')
                    out_file.write(str(rest[0])+',')
                    out_file.write(str(rest[1])+',')
                    out_file.write(str(rest[2])+'\n')
    except:
        fails += 1
