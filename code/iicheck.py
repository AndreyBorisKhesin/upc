from math import sqrt
x = 253
with open('../data/'+str(x)+'.csv', 'r') as eph_file:
    with open('../analysis/'+str(x)+'planar.csv', 'w') as out_file:
        out_file.write('et,mt,d\n')
        eph_file.readline()
        mini = -1
        for start in eph_file:
            dat = start.split(',')
            et = float(dat[0])
            erx = float(dat[1])
            ery = float(dat[2])
            erz = float(dat[3])
            evx = float(dat[4])
            evy = float(dat[5])
            evz = float(dat[6])
            with open('../data/mars.csv', 'r') as mars_file:
                mars_file.readline()
                for end in mars_file:
                    mdat = end.split(',')
                    mt = float(mdat[0])
                    mrx = float(mdat[1])
                    mry = float(mdat[2])
                    mrz = float(mdat[3])
                    mvx = float(mdat[4])
                    mvy = float(mdat[5])
                    mvz = float(mdat[6])
                    hx = ery * evz - erz * evy
                    hy = erz * evx - erx * evz
                    hz = erx * evy - ery * evx
                    Rrx = mry * erz - mrz * ery
                    Rry = mrz * erx - mrx * erz
                    Rrz = mrx * ery - mry * erx
                    hR = hx * mrx + hy * mry + hz * mrz
                    Rr = sqrt(Rrx*Rrx + Rry*Rry + Rrz*Rrz)
                    dmax = hR/Rr
                    if mini == -1:
                        mini = dmax
                    if dmax < 0:
                        dmax = -dmax
                    if dmax < 1 and mt > et and dmax < mini:
                        out_file.write(str(et)+','+str(mt)+','+str(dmax)+'\n')
                        mini = dmax
