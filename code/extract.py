fails = 0
masses = {}
energy = []
with open('../data/mass.csv', 'r') as mass_file:
    mass_file.readline()
    for line in mass_file:
        seg = line.split(',')
        masses[int(seg[0])] = float(seg[2])
for i in range(1, 1000):
    try:
        with open('../data/outf/'+str(i)+'.csv', 'r') as eph_file:
            eph_file.readline()
            bestE = ["-1", "-1", "-1", "-1"]
            bestI = ["-1", "-1", "-1", "-1"]
            bestD = ["-1", "-1", "-1", "-1"]
            for p in eph_file:
                segs = p.split(',')
                t = segs[0]
                t2 = segs[1]
                E = segs[2]
                if E == -1:
                    continue
                I = segs[3]
                if I == -1:
                    continue
                if float(E) > float(bestE[1]) or bestE[1] == "-1":
                    bestE[0] = t2
                    bestE[1] = E
                    bestE[2] = I
                    bestE[3] = t
                if float(I) < float(bestI[2]) or bestI[2] == "-1":
                    bestI[0] = t2
                    bestI[1] = E
                    bestI[2] = I
                    bestI[3] = t
                if (float(E) - float(I) > float(bestD[1]) - float(bestD[2]) or
                   bestD[1] == "-1"):
                    bestD[0] = t2
                    bestD[1] = E
                    bestD[2] = I
                    bestD[3] = t
            if i in masses:
                energy.append(str(i)+','+bestE[3]+','+bestE[0]+',' +
                              str(float(bestE[1])*masses[i]) +
                              ','+str(float(bestE[2])*masses[i])+'\n')
                energy.append(str(i)+','+bestI[3]+','+bestI[0]+',' +
                              str(float(bestI[1])*masses[i]) +
                              ','+str(float(bestI[2])*masses[i])+'\n')
                energy.append(str(i)+','+bestD[3]+','+bestD[0]+',' +
                              str(float(bestD[1])*masses[i]) +
                              ','+str(float(bestD[2])*masses[i])+'\n')

    except(FileNotFoundError):
        fails += 1
with open('../data/energy.csv', 'w') as out_file:
    out_file.write('a,t,t2,E,I\n')
    for x in range(len(energy)):
        out_file.write(energy[x])
