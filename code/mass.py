from math import pi

fails = 0
masses = []
for i in range(1, 4000):
    try:
        with open('../data/massless/'+str(i)+'.eph', 'r') as eph_file:
            for line in eph_file:
                index = line.find("RAD")
                if index >= 0:
                    snippet = line[index+5:]
                    num = float(snippet.split(' ')[0])*1000
                    newnum = 4/3*pi*num*num*num*2638.15
                    newernum = newnum*2.99794*10**12
                    masses.append(str(i)+","+str(newnum) +
                                  ","+str(newernum)+'\n')
                    break
    except:
        fails += 1
with open('../data/mass.csv', 'w') as out_file:
    out_file.write('a,m,E/v^2\n')
    for x in range(len(masses)):
        out_file.write(masses[x])
