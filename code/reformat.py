fails = 0
for i in range(1, 1000):
    try:
        with open('../data/out/'+str(i)+'.csv', 'r') as eph_file:
            with open('../data/outf/'+str(i)+'.csv', 'w') as out_file:
                for line in eph_file:
                    index = line.find(",")
                    if index > 0:
                        out_file.write(line[:index])
                    else:
                        out_file.write(line)
    except:
        fails += 1
