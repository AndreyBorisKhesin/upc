mars_lines = []
with open('../data/mars.eph', 'r') as mars_file:
    start = False
    for line in mars_file:
        if line.startswith("$$EOE"):
            start = False
        if start:
            mars_lines.append(line.strip().replace('  ', ' '))
        if line.startswith("$$SOE"):
            start = True
mars_x = {}
mars_y = {}
mars_z = {}
mars_vx = {}
mars_vy = {}
mars_vz = {}
mars_rg = {}
mars_rr = {}
for i in range(len(mars_lines)//4):
    num = int(float(mars_lines[4*i][:mars_lines[4*i].find("=")]))
    xyz = mars_lines[4*i+1].split(' ')
    v = mars_lines[4*i+2].split(' ')
    rest = mars_lines[4*i+3].split(' ')
    mars_x[num] = float(xyz[0])
    mars_y[num] = float(xyz[1])
    mars_z[num] = float(xyz[2])
    mars_vx[num] = float(v[0])
    mars_vy[num] = float(v[1])
    mars_vz[num] = float(v[2])
    mars_rg[num] = float(rest[1])
    mars_rr[num] = float(rest[2])
with open('../data/closest.csv', 'w') as out_file:
    fails = 0
    for i in range(1, 100):
        try:
            eph_lines = []
            with open('../data/'+str(i)+'.eph', 'r') as eph_file:
                start = False
                for line in eph_file:
                    if line.startswith("$$EOE"):
                        start = False
                    if start:
                        eph_lines.append(line.strip().replace('  ', ' '))
                    if line.startswith("$$SOE"):
                        start = True
            x = {}
            y = {}
            z = {}
            vx = {}
            vy = {}
            vz = {}
            rg = {}
            rr = {}
            for j in range(len(eph_lines)//4):
                num = int(float(eph_lines[4*j][:eph_lines[4*j].find("=")]))
                xyz = eph_lines[4*j+1].split(' ')
                v = eph_lines[4*j+2].split(' ')
                rest = eph_lines[4*j+3].split(' ')
                x[num] = float(xyz[0])
                y[num] = float(xyz[1])
                z[num] = float(xyz[2])
                vx[num] = float(v[0])
                vy[num] = float(v[1])
                vz[num] = float(v[2])
                rg[num] = float(rest[1])
                rr[num] = float(rest[2])
            maxr2 = 0
            for j in x:
                dx = x[j]-mars_x[j]
                dy = y[j]-mars_y[j]
                dz = z[j]-mars_z[j]
                r2 = dx*dx + dy*dy + dz*dz
                if r2 > maxr2:
                    maxr2 = r2
            out_file.write(str(i)+':'+str(maxr2)+'\n')
        except:
            fails += 1
print(fails)
