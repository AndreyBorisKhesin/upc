\documentclass[twoside]{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{fancyhdr}
\usepackage[margin=1in]{geometry}
\usepackage{import}
\usepackage{multicol}
\usepackage[strict]{changepage}
\usepackage{capt-of}
\usepackage{caption}
\captionsetup{labelformat=empty,labelsep=none}
\usepackage{graphicx}
\usepackage{gensymb}
\usepackage{siunitx}
\usepackage[americanvoltage, siunitx]{circuitikz}
\usepackage{mathrsfs}
\usepackage{xspace}
\usepackage[T1]{fontenc}
\usepackage{mathptmx}
\usepackage{apcgraph}
\usepackage{apctable}
\usepackage{listings}

% Indent
\usepackage{indentfirst}
\setlength{\parindent}{0.3in}

\DeclareSIUnit\AU{AU}

% Title section
\pagestyle{fancy}
\thispagestyle{empty}

\renewcommand{\headrulewidth}{0pt}

\newcommand\ptitle[1]{{\phantom{.}\vspace{15em}\centering\fontsize{20pt}{1em}
\textsc{#1}\\\mbox{}\\
[-0.5em]}\fancyhead[OC]{\fontsize{12pt}{12pt}\selectfont\textit{#1}}}

\newcounter{people}

\newcommand\pauthortext[2]{{\centering\fontsize{12pt}{1em}\selectfont
\textsc{#1}\\[0.5em]
\fancyhead[EC]{\fontsize{12pt}{12pt}\selectfont\textit{#2}}}}

\newcommand\pauthor[1]{{\stepcounter{people}
\ifnum\value{people}=1
{\pauthortext{#1}{#1}
\global\def\auth{#1\xspace}}
\else\ifnum\value{people}=2
{\pauthortext{#1}{\auth and #1}}
\else{\pauthortext{#1}{\auth et al.}}\fi\fi}}

\newcommand\pdate[1]{{\vspace{-0.3em}\centering\fontsize{9pt}{1em}\selectfont
\text{(Received #1)}\\[1em]}}

% Page header
\newcommand{\pheader}[1]{\fancyhead[EC]{\fontsize{12pt}{12pt}\selectfont
\textit{#1}}}
\fancyhead[RO, EL]{\fontsize{12pt}{12pt}\selectfont\thepage}
\cfoot{}

\makeatletter
\newenvironment{padjustwidth}[2]{
\begin{list}{}{
\setlength\partopsep\z@
\setlength\topsep\z@
\setlength\listparindent\parindent
\setlength\parsep\parskip
\linespread{0}\selectfont
\@ifmtarg{#1}{\setlength{\leftmargin}{\z@}}
{\setlength{\leftmargin}{#1}}
\@ifmtarg{#2}{\setlength{\rightmargin}{\z@}}
{\setlength{\rightmargin}{#2}}}
\item[]}{\end{list}}
\makeatother

% Abstract environment
\newenvironment{pabstract}
{\begin{padjustwidth}{0.5in}{0.5in}\bgroup\fontsize{9pt}{1em}\selectfont
\hspace{0.5in}}
{\egroup\end{padjustwidth}\vfill\pagebreak}

% Report environment
\setlength\columnsep{0.5in}
\newenvironment{paper}
{\begin{multicols*}{2}\bgroup\fontsize{12pt}{12pt}\selectfont}
{\egroup\end{multicols*}}

%Sources
\newsavebox{\sourcebox}
\newcommand{\psource}[1]{%
\vspace{-2em}
\text{}\\*
\fontsize{9pt}{10.35}\selectfont
\noindent\renewcommand{\labelenumi}{}
\savebox{\sourcebox}{\parbox{3in}{\begin{enumerate}
%\vspace{-\baselineskip}
\setlength{\leftmargini}{-1ex}
\setlength{\leftmargin}{-1ex}
\setlength{\labelwidth}{0pt}
\setlength{\labelsep}{0pt}
\setlength{\listparindent}{0pt}
\item\textit{\hspace{-0.35in}#1}
\end{enumerate}}}
\usebox{\sourcebox}
}

% Section headers
\newcounter{psectioncounter}
\newcounter{psubsectioncounter}[psectioncounter]
\newcommand\psection[1]{
\stepcounter{psectioncounter}
{\begin{center}\Roman{psectioncounter} \textsc{#1}\end{center}}}
\newcommand\psubsection[1]{\stepcounter{psubsectioncounter}
{\begin{center}\Roman{psectioncounter}.\Roman{psubsectioncounter}
\textsc{#1}\\[0.5em]\end{center}}}

%equation
\newcounter{pequationcounter}
\newcommand\pequation[3]{{
\stepcounter{pequationcounter}
\mbox{}\vspace{-0.75em}
\begin{equation*}#1\tag*{\fontsize{12pt}{1em}\selectfont
$\begin{array}{r}
\cr{\text{[\arabic{pequationcounter}]}}
\cr{\fontsize{9pt}{1em}\selectfont\textit{\ifx\\#2\\~\else(\fi#2\ifx\\#2\\~
\else)\fi}}
\end{array}$}
\end{equation*}}
\expandafter\edef\csname eq#3\endcsname{[\arabic{pequationcounter}]\noexpand
\xspace}}

% Where
\newcommand{\pwherevar}[3]{&$#1$ & #2 \ifx\\#3\\~\else($\smash{\si{\fi#3
\ifx\\#3\\~\else}}$)\fi\\}
\newenvironment{pwhere}[0]
{\bgroup\fontsize{9pt}{9pt}\selectfont Where:\vspace{2pt}\\\begin{tabular}{rr@{ = }p{2.42in}}}
{\end{tabular}\egroup\vspace{12pt}}


%Figure counter
\newcounter{pfigurecounter}
\newcommand{\pcaption}[1]{%
\vspace{-24pt}
\bgroup
\stepcounter{pfigurecounter}
\captionof{figure}{\fontsize{9pt}{9pt}\selectfont
\hspace{0.3in}Fig \arabic{pfigurecounter}. #1}\egroup}

\newcommand\abs[1]{\left\lvert#1\right\rvert}
\newcommand\oo[0]{\infty}
\newcommand\limto[3]{\lim\limits_{#1\rightarrow#2}#3}
\renewcommand\d[0]{\mathrm{d}}
\newcommand\cross[0]{\times}
\newcommand\Mod[1]{\ (\text{mod}\ #1)}
\newcommand\pars[1]{\left(#1\right)}
\newcommand\since{\raisebox{0.75pt}{$\because\,$}}
\newcommand\sqbrack[1]{\left[#1\right]}
\newcommand\R{\mathbb{R}}
\newcommand\di{\partial}
\newcommand\x{\times}
\newcommand\del{\nabla}

\lstdefinestyle{csc148py}
 {belowcaptionskip=1\baselineskip,
  breaklines=true,
  xleftmargin=\parindent,
  language=Python,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\textbf,
  commentstyle=\textit,
  identifierstyle=\textbf,
  %stringstyle=\color{red}
}
