\input{header}
\begin{document}
\ptitle{Terraforming Mars via Asteroid Impacts}
\pauthor{Problem A\@: Terraforming Mars}
\pauthor{Team 200}
\pheader{Team 200}
\pdate{November 15, 2015}
\begin{pabstract}
To terraform Mars,
its atmosphere must become warm enough to allow liquid water to exist.
To do this, \SI{9.6E26}{\joule} of heat need to be generated.
By considering a sample of asteroids,
the amount of impulse needed to collide them with Mars was calculated,
as well as the energy generated.
By taking ten such asteroids,
it was found that a total of
\SI{3.51212E22}{\newton\second}
is needed to redirect the asteroids into Mars.
This is beyond anything available to humans today,
but not beyond anything possible in nature.
\end{pabstract}
\begin{paper}
\psection{Introduction}
Mars, the fourth planet from the Sun,
is the planet most capable of eventually sustaining life in our Solar System,
other than Earth.
Even so, it is not nearly habitable enough to sustain human life.
With its thin carbon dioxide atmosphere, its lack of liquid water,
no magnetic field or ozone layer to protect from harmful radiation,
and its violent planet-wide sandstorms,
Mars is not capable of keeping humans, or even most plants, alive.

However, with the dwindling resources,
constrained space, and increasing population of Earth,
humankind has been seeking a new planet to colonize,
and Mars is the best contender.
But to colonize it, the above issues have to be fixed.

For this, terraforming was proposed.
Terraforming, intentionally causing widespread climate change
on Mars to make it capable of sustaining life,
is a popular, but difficult, idea.
It is needed for long-term survival on Mars,
but most solutions today require an incredible amount of energy.

One of the many proposed approaches for terraforming Mars
is to redirect asteroids,
small rocky and ice bodies in stable orbits around the Sun,
to collide with Mars.
This will heat the atmosphere to create a more stable environment,
allowing for liquid water to exist for prolonged periods of time.
While this will not fully terraform Mars as the air will still be unbreathable,
it can serve as a good start to a solution.

\psection{Problem Statement}
The problem as posed was taken to mean the following:
What is the total impulse that must be applied to ten asteroids to redirect them
into a Mars-intersecting orbit such that their collision would warm Mars to the
point where liquid water could exist in a stable form?
Assumptions that were made include the fact that the impulse is delivered
instantaneously and that the mass of bodies other than the Sun do not affect the
asteroids.

\psection{Method}
To terraform Mars,
a temperature was chosen as the necessary amount to which
the atmosphere must be warmed.
Then, for a large sample of asteroids,
orbits were calculated that would redirect them into Mars.
For these orbits,
the heat released and impulse needed was computed.
Then, ten asteroids were chosen such that the heat released
is enough to reach the temperature chosen earlier.
From this, the amount of total impulse needed was computed.

\psection{Theory}
Since the problem deals with celestial mechanics, instead of metres (m),
kilograms (kg), and seconds (s), units were expressed in terms of
astronomical units (AU), kilograms (kg), and days (d).

The most important equations in celestial mechanics are Kepler's Three Laws.
These laws dictate the behaviour of objects in orbit.
These state the following (HRW 9$\smash{^e}$, 2010).

\begin{enumerate}
\item All objects in orbit travel in conic sections.
\item The area swept out by an object in a given orbit
is a linear function of time, regardless of position.
\item The period of orbit squared
is proportional to the semi-major axis cubed.
\end{enumerate}

A conic section is an ellipse or a hyperbola.
The area swept out by the object is the area swept out by the vector from the
central body to the object, which is also called the radial vector.
The period of orbit is the length of time that it takes for an object to go
around it's elliptical orbit.
The semi-major axis of an ellipse is the distance from its centre to the
farthest point on its boundary.

These law can be expressed symbolically.
The following two equations state Kepler's Second and Third Laws,
respectively.

\pequation{\frac{\d}{\d{t}}r^2\dot\theta=0}{HRW 9$\smash{^e}$, 2010}{KTwo}
\begin{pwhere}
\pwherevar{t}{time spent by an object in orbit}{\day}
\pwherevar{r}{distance to object from orbit centre}{\AU}
\pwherevar{\dot\theta}{time derivative of arc swept by the object}{\per\day}
\end{pwhere}

\pequation{T=2\pi\sqrt\frac{a^3}{GM}}{HRW 9$\smash{^e}$, 2010}{KThree}
\begin{pwhere}
\pwherevar{T}{period of orbit}{\day}
\pwherevar{G}{gravitational constant}{\AU}
\pwherevar{M}{mass of the central star}{\kg}
\pwherevar{a}{semi-major axis of orbit}{\AU}
\end{pwhere}

$G$ was taken as \SI{6.67E-11}{\meter\cubed\per\kilogram\per\second\squared}
and $M$ was taken as \SI{1.99E30}{\kilo\gram}
(HRW 9$\smash{^e}$, 2010).

As stated by Kepler's first law, all orbits are conic sections.
Although it is theoretically possible to have two objects collide while one (the
asteroid) is on an escape trajectory out of the solar system, the analysis was
limited to elliptical orbits.
The polar equation for an ellipse is given by the following relation.

\pequation{r=\frac{a(1-e^2)}{1+e\cos(\theta)}}{Weisstein}{El}
\begin{pwhere}
\pwherevar{e}{eccentricity of orbit}{}
\pwherevar{\theta}{argument from perihelion}{}
\end{pwhere}

Another crucial orbital mechanics equation was used.
It is used to determine the speed of a satellite in orbit with only the mass of
the body it is orbiting, it's distance, and the length of the semi-major axis.
It is called the vis-viva equation.

\pequation{v=\sqrt{GM\pars{\frac2r-\frac1a}}}{Weisstein}{Vv}
\begin{pwhere}
\pwherevar{v}{speed of satellite}{\metre\per\second}
\end{pwhere}

\psection{Analysis}
In order to determine what impulses and asteroids are needed to terraform Mars,
the amount of energy to terraform Mars is needed.
To do this, the heat capacity of Mars' atmosphere and crust was computed.
While only heating of the atmosphere is desired,
heat will also drain into the crust,
meaning it must also be considered.
The heat capacity of the crust was sourced as \SI{1.6E25}{\joule\per\kelvin}
from (M. Szurgot).
The heat capacity of the atmosphere was calculated
by taking the heat capacity of its constituents,
and summing.

\pequation{c_{atm}=m_{atm}\sum\limits_{i\in A}c_{i}P_{i}}{}{MAtm}
\begin{pwhere}
\pwherevar{c_{atm}}{heat capacity of Martian atmosphere}{\joule\per\kelvin}
\pwherevar{m_{atm}}{mass of Martian atmosphere}{\kilo\gram}
\pwherevar{c_{i}}{specific heat capacity of gas $i$}
{\joule\per\kilo\gram\per\kelvin}
\pwherevar{P_{i}}{fraction of atmosphere which is $i$}{}
\pwherevar{A}{set of constituents of atmosphere}{}
\end{pwhere}

From this, the heat capacity of the atmosphere was found to be
many orders of magnitude smaller than the heat capacity of the crust.
Thus, the heat capacity of Mars $(c_{M})$
can be taken as the heat capacity of the crust.

The current average Mars atmospheric temperature
is roughly \SI{220}{\kelvin} (NASA Quest).
For the purposes of terraforming,
the increase in temperature should make Mars
be capable of sustaining liquid water.
Thus, the temperature must be raised to \SI{273.15}{\kelvin}.
To create a slight error margin,
it can instead be raised to \SI{280}{\kelvin}.
Thus, Mars' temperature needs to be increased by \SI{60}{\kelvin}.

Combining this with the above,
to raise the temperature of Mars sufficiently,
\SI{9.6E26}{\joule} of energy need to be generated.

To determine which asteroids could be used for terraforming,
a sampel of 400 asteroids with known orbits were considered.
These asteroids were sourced from the JPL Small Body Database,
using the HORIZONS system.
While all asteroids had incredible exact information for position and velocity,
of the asteroids with classification numbers less than 4000,
only 10 were found to have known mass.
An additional 13 asteroids were sourced from the NASA Space Science Data
Coordinated Archive.

As data on asteroid masses was found to be scarce, the densities of the 23
asteroids whose masses were known were averaged.
The JPL Small Body Database provided all of the radii for all known asteroids.
This allowed for each of the 23 asteroids to be approximated to have a roughly
spherical shape with its average radius as the radius of the sphere.
The formula for the density of a sphere,
derived from the volume of a sphere and the density relation,
was used to calculate the aforementioned density
and the resulting asteroid masses.

\pequation{\rho=\frac{3m}{4\pi R^3}}{}{D}
\begin{pwhere}
\pwherevar{\rho}{density of asteroid}{\kilo\gram\per\AU\cubed}
\pwherevar{m}{mass of asteroid}{\kilo\gram}
\pwherevar{R}{average radius of asteroid}{\AU}
\end{pwhere}

The average density was found to be \SI{2639.15}{\kilo\gram\per\meter\cubed}.
Some of the asteroids used are listed below.
\\\noindent
\bgroup\fontsize{10pt}{10pt}\selectfont
\begin{center}
\begin{apctable}{|[outer]I|[inner]C|[inner]C|[outer]}
\multirow{2}{*}{\textsc{Asteroid}} & \csymb{$Gm$} & \csymb{$r$}\\
& (\si{\AU\cubed\per\day\squared}) & (\si{\AU})\n
1 Ceres      & \num{1.40926E-13} & \num{3.08159E-6}\n
2 Pallas     & \num{3.18461E-14} & \num{1.82489E-6}\n
4 Vesta      & \num{3.85427E-14} & \num{1.75691E-6}\n
21 Lutetia   & \num{2.52983E-16} & \num{3.97789E-7}\n
140 Siwa     & \num{2.23226E-16} & \num{3.44256E-7}\n
433 Eros     & \num{9.95566E-19} & \num{6.57095E-8}\n
1566 Icarus  & \num{1.48814E-22} & \num{4.67921E-9}\n
4979 Otawara & \num{2.97627E-20} & \num{1.83826E-8}\\
\end{apctable}
\end{center}
\egroup
\pcaption{The masses $(Gm)$ and radii $(r)$ of several asteroids.
These are some of the few asteroids with known orbits.
These values were used to compute the density of average asteroids.}

In order to determine the impulse needed to hit Mars at a given moment with a
given asteroid, the new trajectory of the asteroid must be computed.
In order to determine the optimal asteroids to crash into Mars,
the amount of impulse required to make them hit Mars at a given time,
was computed for all times and minimized.
The JPL Small Body Database was used to generate ephemerides for 120 asteroids.
These were taken over the five year interval 2025--01--01 to 2020--01--01 in
intervals of 30 days.
This allowed for the asteroids to fully go around their orbits, while generating
points that were close enough together for there to be no significant difference
in the position and velocity of both Mars and the asteroid.

After being redirected, the asteroid must be on course to collide with Mars.
If the time when the asteroid is redirected and the time of its impact with
Mars were predetermined, this implies that three points on the plane of its
resultant orbit are known.
The ephemerides were used to calculate the position of the asteroid at the time
of redirection and Mars at the time of impact.
From there, the positions of the point where the asteroid is redirected, the
point where it hits Mars, and the Sun were known.
Any three (non-collinear) points lie in a unique plane, so the vectors for the
two points of interest were rotated into the $xy$-plane.
The Sun was always taken to be at the origin.
The rotation was effected with the application of rotational matrices,
by first rotating about $z$ so that one point lay on the $xz$-plane,
and then rotating about $x$ to place both points on the $xy$-plane.
The exact rotation is displayed below.

\bgroup\fontsize{7pt}{1em}\selectfont
\pequation{\vec{v}_{r}=
\begin{pmatrix}
\cos\theta_n  & \sin\theta_n & 0\\
-\sin\theta_n & \cos\theta_n & 0\\
0             & 0            & 1
\end{pmatrix}\begin{pmatrix}
\cos\psi_n  & 0 & -\sin\psi_n\\
0           & 1 & 0\\
\sin\psi_n  & 0 & \cos\psi_n\\
\end{pmatrix}\vec{v}_{0}}{}{RotMat}
\begin{pwhere}
\pwherevar{\vec{v}_{0}}{original vector}{}
\pwherevar{\vec{v}_{r}}{rotated vector}{}
\pwherevar{\psi_n}{yaw of the normal vector to the two positions}{}
\pwherevar{\theta_n}{pitch of the normal vector to the two positions}{}
\end{pwhere}
\egroup

Once the vectors were in the $xy$-plane, their $z$ component became 0.
The following equation was derived from the definition of the dot product,
and was used to find the angle between the vectors.

\pequation{\phi=\cos^{\text-1}\pars{\frac{\vec r_a\cdot\vec r_M}{|\vec r_a|
|\vec r_M|}}}{}{Dot}
\begin{pwhere}
\pwherevar{\phi}{angle between vectors}{}
\pwherevar{\vec r_a}{radial vector of asteroid at time of diversion}{\AU}
\pwherevar{\vec r_M}{radial vector of Mars at time of impact}{\AU}
\end{pwhere}

To solve for the elliptical orbit that passes through the two points, \eqEl was
used.
It is clear from \eqEl that the product of the $r$ and $(1+e\cos(\theta))$ is a
constant for a given ellipse.
The the perihelion of the ellipse that was fitted to those two points is not
aligned with the $x$-axis, thus it appeared rotated.
To fix this, \eqEl was used to solve for the angle between the asteroid and the
perihelion.
This resulted in the following equation which was solved by \textit{Wolfram Mathematica 10}.

\pequation{\fontsize{11pt}{1em}\selectfont|\vec r_a|(1+e\cos(\tau))=|\vec r_M|
(1+e\cos(\tau+\phi))}{}{TauOne}
\begin{pwhere}
\pwherevar{\tau}{argument $\vec r_a$ from perihelion}{}
\end{pwhere}

The solution to this equation was evaluated to be the following.
For simplicity, the equation was expressed in terms of placeholder variables.

\pequation{\fontsize{8pt}{1em}\selectfont\tau=\cos^{\text-1}
\pars{\frac{p(m-n\cos(\phi))+|n\sin(\phi)|\pm\sqrt{q^2-p^2}}{q^2}}}{}{TauTwo}
\begin{pwhere}

\pwherevar{m}{$e|\vec r_a|$}{\AU}
\pwherevar{n}{$e|\vec r_M|$}{\AU}
\pwherevar{p}{$|\vec r_M|-|\vec r_a|$}{\AU}
\pwherevar{q}{$e|\vec r_M-\vec r_a|$}{\AU}
\end{pwhere}

It was noted that \eqTauTwo contained a plus minus.
The reason for this is because there are two ways to fit an ellipse with a given
eccentricity: one where the the orbit is larger, but the points are located near
the perihelion, and vice versa.
However, the effected computations took a lot more time if the sign inside
$\tau$ was set to positive.
The resulting orbits tended to plot a path that went really far away and back,
really quickly.
For these reasons, these larger orbits were ignored and $\tau$ was always set to
contain a negative sign.
Given $\tau$, it is possible to compute the semi-major axis of the fitted
ellipse in terms of the eccentricity.
This was done simply by rearranging \eqEl and plugging in $\tau$ which is the
argument of $\vec r_a$ with respect to the perihelion.

\pequation{a=\frac{|\vec r_a|(1+e\cos(\tau))}{1-e^2}}{}{A}

The ellipse, $\vec r_a$, and $\vec r_M$ were then rotated clockwise by
$\arg(\vec r_a)-\tau$ to align the perihelion with the $x$-axis.
Next, the ellipse and the two vectors were translated in the positive $x$
direction, by $ea$, aligning the centre of the ellipse with the origin.
Finally, the ellipse was turned into a circle by dilating all of the
$x$-coordinates by a factor of $\frac ba$ where $b$ is the semi-minor axis.
It is important to note that $\frac ba$ is equal to $\sqrt{1-e^2}$.
Once the ellipse had been transformed into a circle, \eqKTwo was applied to
calculate the period.

Kepler's second law states that equal areas from the satellite to the focus are
swept out in equal times.
This meant that the ratio of the area swept out on the circle to the area of the
circle will be equal to the ratio of the time of flight to the impact point to
the period of the asteroid's orbit.
This is only true because rotation, translation, and dilation preserve area
ratios.
The area swept out on a circle from a dilated focus point was equal to that
of the area swept out from the centre with the addition or subtraction of two
triangles formed by the centre, the focus, and the orbiting body.
The sign with which the triangle areas are taken is equal to the sign of the
$y$-component of the orbiting body radial vector.
To find the area of a sector of a circle,
the area is multiplied by the fractional part of the circumference,
being directly proportional.
This gives the following equation.

\pequation{A_\theta=\frac\theta2r^2}{}{S}
\begin{pwhere}
\pwherevar{A_\theta}{area of sector of angle $\theta$}{\AU\squared}
\pwherevar{\theta}{angle of sector}{}
\pwherevar{r}{radius of circle}{\AU}
\end{pwhere}

To find the period of the orbit, the semi-major axis was plugged into
\eqKThree.
The period was then plugged into the following equation to calculate the ratio
described above.

\pequation{t=T\frac{\frac\theta2b^2+\frac{ea}2\sqrt{1-e^2}(\vec r'_{a,y}-\vec r'_{M,y})}
{\pi b^2}}{}{TOne}
\begin{pwhere}
\pwherevar{t}{time it takes for the asteroid to hit Mars}{\day}
\pwherevar{b}{semi-minor axis}{\AU}
\pwherevar{\vec r'_{a,y}}{$y$-component of shifted asteroid position}{\AU}
\pwherevar{\vec r'_{M,y}}{$y$-component of shifted Mars position}{\AU}
\end{pwhere}

However, \eqKThree can be used to simplify \eqTOne to the following equation.

\pequation{\fontsize{10pt}{1em}\selectfont t=\frac{\theta b^2+ea\sqrt{1-e^2}
(\vec r'_{a,y}-\vec r'_{M,y})}{b^2}\sqrt{\frac{a^3}{GM}}}{}{TTwo}

It is important to note that both $a$ and $t$ are functions of $e$.
In these equations, $\vec v'$ was used to denote the rotated vector $\vec v$,
aligned so that the perihelion of its orbit lay on the positive $x$-axis.
Additionally, $\vec v_y$ was used to denote the $y$-component of $\vec v$.

Since the ellipse has been transformed into a circle, the magnitudes
$|\vec r'_a|$ and $|\vec r'_M|$ are identical and equal to the dilated value of
$a$ as well as the unchanged value of $b$.

It can be seen from \eqTTwo that it is easy to find the time of flight from the
eccentricity but since the function does not have an inverse, it is difficult to
compute the eccentricity from the time of flight.
A sample plot of the time of flight as a function of eccentricity can be seen
below.
It is for the vectors $\vec r_a=(3,2,0)\si{\AU}$
and $\vec r_M=(-2,2,0)\si{\AU}$.
\\\noindent
\begin{center}
\begin{tikzpicture}
\begin{apcgraph}{Time vs. Eccentricity}
{$e$}{$t$(\si{\day})}{0}{1}{0}{10000}{1}{0}
\addplot [color=black,no marks
%error bars/.cd,
%y dir=both,y fixed=5,
%x dir=both,x fixed=.00005
]
table [y=t,x=e,col sep=comma] {ellipse1.csv};
\end{apcgraph}
\end{tikzpicture}
\end{center}
\pcaption{If two points in space are chosen and an eccentricity ($e$)
are chosen, an orbit can be drawn through the points
with the given $e$.
This show the time between the two points for a given $e$ and points.}

The plot does not extend past a certain minimal eccentricity.
This is when the orbit becomes too circular to be able to pass through two
points at different distances from the Sun.
This point is where the two possible orbits that can be generated (by changing
the sign in $\tau$) are identical.
This means that the expression inside the square root is equal to 0.
The above was rearranged so $e$ can be no smaller than its value in the
following equation.

\pequation{e_{min}=\frac{|\vec r_M|-|\vec r_a|}{|\vec r_M-\vec r_a|}}{}{E}
\begin{pwhere}
\pwherevar{e_{min}}{the smallest value that the eccentricity can have}{}
\end{pwhere}

The eccentricity does not have an upper bound.
The reason for this is because an eccentricity that is slightly smaller than 1
is indicative of an ellipse that is practically a straight line, along which the
asteroid travels, taking an arbitrarily large amount of time.

A program was created to calculate these eccentricities.
It analyzed 120 asteroids from the JPL Small Body Database by considering every
possible time at which the asteroid can be diverted and every possible time at
which the asteroid crashes into Mars.
The program, given the vector at which the asteroid is diverted, the vector
where the asteroid hits Mars, and the time between the two events, looked for an
eccentricity that would result in the given time.

This is effectively the same as finding roots of the time function from which
the desired time has been subtracted.
Upper and lower bounds were set on the eccentricity at the maximum and minimum
of the time function.
This can be done using Newton's Method of Approximation.
The average of the upper and lower bounds was chosen as a first guess, so
$x_1$ was set to $\frac{1+e_{min}}2$.
Every subsequent guess is determined by the Newton's Method formula.

\pequation{x_{i+1}=x_i-\frac{t(x_i)}{t'(x_i)}}{}{NOne}
\begin{pwhere}
\pwherevar{x_i}{the $i^\text{th}$ guess}{}
\end{pwhere}

The derivative of the function was not computed algebraically.
It was calculated by taking a local linear approximation using the formula of
first principles.

\pequation{f'(x)=\limto{h}{0}{\frac{f(x+h)-f(x)}h}}{}{FP}

In the program, $h$ was taken to be $(1-e_{min})10^{\text-6}$.
Smaller values of $h$ did not change results in any noticeable way so the above
was deemed sufficient.
Combining \eqNOne and \eqFP results in the following equation.

\pequation{x_{i+1}=x_i-\frac{ht(x_i)}{t(x_i+h)-t(x_i)}}{}{NTwo}

Once the eccentricity was computed, the semi-major axis could be computed.
With the use of \eqVv, the allowed for the velocity at redirection and at impact
to be computed.
The computation of the velocity vector was effected by scaling a unit vector by
the magnitude of the velocity vector, the latter of which was obtained using
\eqVv.

To compute the velocity direction, it is sufficient to compute the slope of the
tangent line which is the ratio of the derivatives of the position vector's $y$
and $x$ components by the angle.
The $x$ and $y$ components can be expressed as $r\cos(\theta)$ and
$r\sin(\theta)$, respectively.
To express $r$ in terms of $\theta$, \eqEl was used.
For the $x$-component, the following equation was obtained.

\pequation{\frac{\d x}{\d\theta}=\frac{a(e^2-1)\sin(\theta)}
{(1+e\cos(\theta))^2}}{}{Dx}

For the $y$-component, the following equation was obtained.

\pequation{\frac{\d y}{\d\theta}=\frac{a(e^2-1)(-e-\cos(\theta))}
{(1+e\cos(\theta))^2}}{}{Dy}

Taking the ratio of \eqDy and \eqDx the slope of the tangent line is obtained.

\pequation{\frac{\d y}{\d x}=\frac{-e-\cos(\theta)}{\sin(\theta)}}{}{V}

The velocity of Mars at the point of impact was subtracted from the resulting
impact velocity to obtain the relative velocity at impact.
Squaring its magnitude and multiplying it by half the mass yielded the energy
with which the asteroid strikes Mars.
The velocity of the asteroid before the redirection was subtracted from the
resulting velocity after redirection to obtain the change in velocity that needs
to be imparted to the asteroid.
Multiplying its magnitude by the mass yielded the impulse that must be imparted
to it.

To verify the correctness of this algorithm,
it was tested with Mars.
An object, at Mars' position at some time $t$ was taken as the starting position,
with the target being an object at Mars' position at some time $t+\Delta t$.
The algorithm was used to compute the required velocity
to travel between these points in $\Delta t$ time.
The resulting velocity was very close to the actual sourced value from NASA\@.
This served as an indicator that this algorithm was indeed correct.

The above method was then used to compute the best paths available
to get from the current orbit to Mars,
also computing the energy released at impact.
Note that at impact, 20\% of the energy is released as light
(D. Yeomans \& P. Chodas).
The remainder of the energy was assumed to remain on the planet as heat energy.
Thus, the amount of energy released by each satellite can be computed.
Below are the 10 asteroids who release the closest
to 0.1 of the required heat.
By crashing these into Mars,
a temperature close to the desired will hopefully be attained.
\\\noindent
\bgroup\fontsize{10pt}{10pt}\selectfont
\begin{center}
\begin{apctable}{|[outer]I|[inner]C|[inner]C|[outer]}
\multirow{2}{*}{\textsc{Asteroid}} & \csymb{$E$} & \csymb{$J$}\\
& (\si{\joule}) & (\si{\newton\second})\n
310 Margarita & \num{1.182E26} & \num{1.825E21}\n
314 Rosalia   & \num{1.336E26} & \num{3.575E21}\n
336 Lacadiera & \num{1.336E26} & \num{6.207E21}\n
339 Dorothea  & \num{1.175E26} & \num{1.041E21}\n
384 Burdigala & \num{1.323E26} & \num{5.491E20}\n
394 Arduina   & \num{1.133E26} & \num{2.286E21}\n
400 Ducrosa   & \num{1.185E26} & \num{1.995E21}\n
413 Edburga   & \num{1.005E26} & \num{1.291E21}\n
515 Athalia   & \num{1.329E26} & \num{7.121E20}\n
522 Helga     & \num{1.188E26} & \num{1.564E22}\n
\end{apctable}
\end{center}
\egroup
\pcaption{The energy upon release and impulse needed for several asteroids.
These are the ten asteroids used for the final calculation.}

With the 20\% loss,
these asteroids together provide
\SI{9.7536E26}{\joule}.
This translates to a \SI{60.96}{\kelvin}
increase in average temperature.

Thus, these 10 asteroids colliding with Mars should be enough
to begin its terraforming process.

The total impulse needed to redirect these 10 asteroids is
\SI{3.51212E22}{\newton\second}.

After the required change in momentum was computed,
a Saturn V, the most powerful rocket humanity has built so far,
was considered.
Its exhaust velocities and fuel masses were sourced from
\textit{Encylopedia Astronautica}.
By using these, the total momentum a Saturn V could obtain was calculated.
For this total momentum, and the mass of an asteroid,
the greatest possible change in velocity for a given asteroid was computed.
This was found to be between \SI{1E-6}{\meter\per\second}
and \SI{1E-3}{\meter\per\second}, depending on the asteroid.
Given that most asteroids have velocities on the order of
\SI{10}{\meter\per\second} or \SI{100}{\meter\per\second},
this is an insignificant amount.
Furthermore, the impulse generated by a single Saturn V
is many orders of magnitude below the required amount.
Thus, a Saturn V cannot cause any major changes in the orbit of an asteroid.

Since the Saturn V proved woefully insufficient, a different approach must be
taken to provide the necessary energy.
This clearly meant that the fuel cannot be carried from Earth.
A hypothetical device that attaches itself to the asteroid was proposed.
It operates by converting the asteroid's mass into energy by splitting its atoms
and unleashing their rest energy which is equal to $mc^2$
(HRW 9$\smash{^e}$, 2010) where
$m$ is the mass converted and $c$ is the speed of light which was taken to be
\SI{2.99792458E8}{\meter\per\second}.
This energy can be used to send a tiny piece of asteroid shooting out the back.
The law of conservation of momentum will send the asteroid forwards.
The coefficient of $c^2$ is so large that a tiny fraction of the asteroid's mass
is sufficient to drastically increase the asteroid's speed.
However, the energy derived is proportional to the square of the velocity.
While converting energy uses up the mass, the momentum acquired increases the
velocity with non-diminishing returns (until relativity comes into play).
As the mass decreases, the velocity will have to go up even more to preserve
momentum.
This means that to get the most energy out of the asteroid, all but a pebble of
its mass must be converted into energy.
Shortly, all that remains is a relativistic pebble with a stupendous amount of
velocity.
The pebble becomes comparable to the Oh-My-God particle which was recorded to be
moving at a speed of $0.9999999999999999999999951c$ (J. Walker).
The goal was to terraform Mars and not destroy it, so alternative methods were
considered.

\psection{Strengths and Weaknesses}
The methods of computation covered in this paper
have both strengths and weaknesses.
Among its greatest of strengths is volume of information.
Being able to download vast amounts of data through HORIZONS,
and then analysing this data through the use
of the programs listed in the Appendix
allowed for a great amount of data to be processed.
This meant that conclusions were more likely to be accurate,
and not incredible affected by outliers.

One of the weaknesses of this method is that
it requires several assumptions.
For example, most asteroids do not have known masses.
This was solved by assuming that asteroids are roughly spherical,
and are all equally dense.
While this is a reasonable assumption,
it could still throw off data.
Another assumption is that the only object affecting the asteroids is the sun.
In reality, it could have its trajectory altered by many objects,
including Jupiter, other asteroids and Mars.

Another strength is that this method could easily be adapted.
Although only the impulse and collision energy were used at the end,
the programs could be reconfigured to return the exact path,
the velocity at a given time or any other piece of information.
Similarly, it could be adapted to a multiple or a continuous burn situation.

\psection{Conclusion}
To terraform Mars,
its atmosphere must be sufficiently warmed.
If the atmosphere must be warmed to a temperature
which can support liquid water,
\SI{9.6E26}{\joule} of heat need to be generated.
By analyzing the orbits and masses of many known asteroids,
ten asteroids were chosen to be redirected into Mars.
The total impulse needed for this was determined to be
\SI{3.51212E22}{\newton\second}.
By comparing this with the maximum impulse generated by a Saturn V,
it was found that a Saturn V could not possibly be enough to terraform Mars.
Thus, to terraform Mars via collisions of asteroids,
revolutionary propulsion systems far beyond those that are available today
are needed.

\psection{Sources}
\psource{M. Szurgot, Heat Capacity of Mars, 2012,
Center of Mathematics and Physics, Technical University of Lodz}
%\psource{http://www.lpi.usra.edu/meetings/marsmantle2012/pdf/6001.pdf}
\psource{Weisstein, Eric W.\@ ``Ellipse.''
From MathWorld --- A Wolfram Web Resource.
http://mathworld.wolfram.com/Ellipse.html}
\psource{Weisstein, Eric W.\@ ``Vis-Viva Equation.''
From ScienceWorld --- A Wolfram Web Resource.
http://scienceworld.wolfram.com/physics/Vis-VivaEquation.html}
%\psource{http://quest.nasa.gov/aero/planetary/mars.html}
\psource{Halliday et al, Fundamentals of Physics $9^{\text{th}}$ Edition,
2010, John Wiley and Sons}
\psource{``Saturn V'', Encylcopedia Astronautica.
http://www.astronautix.com/lvs/saturnv.htm}
\psource{D. Yeomans \& P. Chodas,
``Additional Details on the Large Fireball Event over Russia on Feb. 15, 2013'',
NASA/JPL Near-Earth Object Program Office}
\psource{J. Walker, ``The Oh-My-God Particle'', 1994, Fourmilab}
\end{paper}
\pagebreak
\psection{Python Code}
\lstinputlisting[language=Python, style=csc148py]{code/vcalcs.py}
\pagebreak
\psection{Mathematica Code}
\lstinputlisting[language=Mathematica, breaklines=true]{analysis/orbit.txt}
\end{document}
