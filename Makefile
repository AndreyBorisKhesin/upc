TEX=pdflatex -interaction=batchmode -shell-escape
OPEN=open

.PHONY: clean

clean:
	rm *.aux *.log *.toc *.out *.auxlock

%: %.tex
	$(TEX) $< && $(OPEN) $@.pdf
